package ac.ritsumei.cs.ubi.ibeacon;

import android.app.Activity;
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static android.util.Log.e;
import static android.util.Log.i;

public class MainActivity extends Activity {
    boolean misShort = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        misShort = misShort ? false : true;
        return true;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public class PlaceholderFragment extends Fragment implements CompoundButton.OnCheckedChangeListener{

        BluetoothAdapter mBluetoothAdapter = null;
        BluetoothManager bluetoothManager = null;
        TextView textview;
        Switch record_switch;
        Timer mTimer = null;
        Handler mHandler = new Handler();
        View rootView;
        Integer second, mLaptime;
        double longitude;
        double latitude;

        SQLiteDatabase mydb;
        MySQLiteOpenHelper hlpr;
        String[][] bleDeviceMap;
        final String DB = getExternalFilesDir("btdata") + "/AndroidBLEMining.db";
        static final int DB_VERSION = 1;
        //static final String CREATE_TABLE = "create table calibrationData ( _id integer primary key autoincrement, data integer not null );";
        static final String CREATE_TABLE = "create table calibrationData ( id INTEGER PRIMARY KEY AUTOINCREMENT,time TEXT,essid TEXT,bssid TEXT,lat REAL,lon REAL,rssi INTEGER );";
        static final String DROP_TABLE = "drop table calibrationData;";

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            EditText editSecond = (EditText)rootView.findViewById(R.id.edit_second);
            EditText editLatitude = (EditText)rootView.findViewById(R.id.edit_latitude);
            EditText editLongitude = (EditText)rootView.findViewById(R.id.edit_longitude);
            if(b) {
                second = Integer.parseInt(editSecond.getText().toString() + "0") / 10 >= 1 ?
                        (1000 * Integer.parseInt(editSecond.getText().toString())) : 1000;
                latitude = Double.valueOf(editLatitude.getText().toString());
                longitude = Double.valueOf(editLongitude.getText().toString());
            }else{
                second = 0;
            }
        }

        private class MySQLiteOpenHelper extends SQLiteOpenHelper {
            public MySQLiteOpenHelper(Context c) {
                super(c, DB, null, DB_VERSION);
            }
            public void onCreate(SQLiteDatabase db) {
                db.execSQL(CREATE_TABLE);
            }
            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
                db.execSQL(DROP_TABLE);
                onCreate(db);
            }
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {

            bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            mBluetoothAdapter = bluetoothManager.getAdapter();
            mBluetoothAdapter.startLeScan(mLeScanCallback);
            mLaptime = 0;
            second = 0;
            mTimer = new Timer(true);
            bleDeviceMap = new String[20][4];
            e("are", DB);

            hlpr = new MySQLiteOpenHelper(getApplicationContext());
            mydb = hlpr.getWritableDatabase();

            rootView = inflater.inflate(R.layout.fragment_main, container, false);

            textview = (TextView)rootView.findViewById(R.id.StetusTextView);

            // トグルスイッチのインスタンス生成
            record_switch = (Switch)rootView.findViewById(R.id.record_switch);
            record_switch.setOnCheckedChangeListener(PlaceholderFragment.this);

            mTimer.schedule( new TimerTask(){
                @Override
                public void run() {
                    boolean post = mHandler.post(new Runnable() {
                        public void run() {

                            String bleStatus = "";
                            int strongestElement = -1;

                            if(0 < second) {
                                if(second.equals(mLaptime)){
                                    for (int i = 0; i < 20; i++) {
                                        if (null == bleDeviceMap[i][0]) break;

                                        SQLiteStatement stmt = null;
                                        mydb.execSQL("INSERT INTO calibrationData(time, essid, bssid, lat, lon, rssi) VALUES(?, ?, ?, ?, ?, ?);",
                                                new Object[]{(String)DateFormat.format("yyyy-MM-dd kk:mm:ss", Calendar.getInstance()),
                                                        bleDeviceMap[i][1] + ":" + bleDeviceMap[i][2] + ":" + bleDeviceMap[i][0],
                                                        bleDeviceMap[i][1] + ":" + bleDeviceMap[i][2] + ":" + bleDeviceMap[i][0],
                                                        latitude,
                                                        longitude,
                                                        Integer.valueOf(bleDeviceMap[i][3])
                                                });
                                    }
                                    mLaptime = 0;
                                }else{
                                    mLaptime += 1000;
                                }
                            }
                            for (int i = 0; i < 20; i++) {
                                if (null == bleDeviceMap[i][0]) break;
                                if (strongestElement == -1)
                                    strongestElement = i;
                                if (Integer.parseInt(bleDeviceMap[strongestElement][3]) < Integer.parseInt(bleDeviceMap[i][3]))
                                    strongestElement = i;

                                bleStatus += (misShort ? bleDeviceMap[i][0].substring(0, 9) + "/~/" + bleDeviceMap[i][0].substring(25) : bleDeviceMap[i][0]) + " : " +
                                        bleDeviceMap[i][1] + " : " +
                                        bleDeviceMap[i][2] + "(" +
                                        String.valueOf(Integer.parseInt(bleDeviceMap[i][2], 9), 16) + ") : " +
                                        bleDeviceMap[i][3] + "\n";
                            }
                            if (strongestElement != -1) {
                                bleStatus = "Strongest:\n" +
                                        (misShort ? bleDeviceMap[strongestElement][0].substring(0, 9) + "/~/" + bleDeviceMap[strongestElement][0].substring(25) : bleDeviceMap[strongestElement][0]) + " : " +
                                        bleDeviceMap[strongestElement][1] + " : " +
                                        bleDeviceMap[strongestElement][2] + "(" +
                                        String.valueOf(Integer.parseInt(bleDeviceMap[strongestElement][2], 16)) + ") : " +
                                        bleDeviceMap[strongestElement][3] + "\n\n" + bleStatus;
                            }

                            if(0 == second)
                                textview.setText(bleStatus + "Not Recording");
                            else
                                textview.setText(bleStatus + "Now Recording!");

                            for (int i = 0; i < 20; i++) {
                                if (null == bleDeviceMap[i][0]) break;
                                bleDeviceMap[i][0] = null;
                            }
                        }
                    });
                }
            }, 1000, 1000);
            i("info", "Created view");

            return rootView;
        }

        private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(final BluetoothDevice device, int rssi,byte[] scanRecord) {
                // デバイスが検出される度に呼び出されます。

                if(scanRecord.length > 30)
                {
                    if((scanRecord[5] == (byte)0x4c) && (scanRecord[6] == (byte)0x00) &&
                            (scanRecord[7] == (byte)0x02) && (scanRecord[8] == (byte)0x15))
                    {
                        String uuid = IntToHex2(scanRecord[9] & 0xff)
                                + IntToHex2(scanRecord[10] & 0xff)
                                + IntToHex2(scanRecord[11] & 0xff)
                                + IntToHex2(scanRecord[12] & 0xff)
                                + "-"
                                + IntToHex2(scanRecord[13] & 0xff)
                                + IntToHex2(scanRecord[14] & 0xff)
                                + "-"
                                + IntToHex2(scanRecord[15] & 0xff)
                                + IntToHex2(scanRecord[16] & 0xff)
                                + "-"
                                + IntToHex2(scanRecord[17] & 0xff)
                                + IntToHex2(scanRecord[18] & 0xff)
                                + "-"
                                + IntToHex2(scanRecord[19] & 0xff)
                                + IntToHex2(scanRecord[20] & 0xff)
                                + IntToHex2(scanRecord[21] & 0xff)
                                + IntToHex2(scanRecord[22] & 0xff)
                                + IntToHex2(scanRecord[23] & 0xff)
                                + IntToHex2(scanRecord[24] & 0xff);

                        String major = IntToHex2(scanRecord[25] & 0xff) + IntToHex2(scanRecord[26] & 0xff);
                        String minor = IntToHex2(scanRecord[27] & 0xff) + IntToHex2(scanRecord[28] & 0xff);
                        i("BTLE", major + ":" + minor + ":" + rssi);
                        final String[] beacon = {uuid, major, minor, String.valueOf(rssi)};
                        new Thread(new Runnable() {
                            public void run() {
                                mHandler.post(new Runnable() {
                                    public void run() {
                                        for(int i=0;i<20;i++) {
                                            if (beacon[0].equals(bleDeviceMap[i][0]) && beacon[1].equals(bleDeviceMap[i][1]) &&
                                                    beacon[2].equals(bleDeviceMap[i][2])) break;
                                            if (null == bleDeviceMap[i][0]) {
                                                bleDeviceMap[i][0] = beacon[0];
                                                bleDeviceMap[i][1] = beacon[1];
                                                bleDeviceMap[i][2] = beacon[2];
                                                bleDeviceMap[i][3] = beacon[3];
                                                break;
                                            }
                                        }
                                    }
                                });
                            }
                        }).start();
                    }
                }
            }
        };

    }

    private String IntToHex2(int Value) {
        char HEX2[]= {Character.forDigit((Value>>4) & 0x0F,16),
                Character.forDigit(Value & 0x0F,16)};
        String Hex2Str = new String(HEX2);
        return Hex2Str.toUpperCase();
    }
}
